const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

updateViewersCount = (snapshot, context) => {
    const viewersRef = admin.database().ref(`/viewers/${context.params.classroomId}`);
    const counterRef = admin.database().ref(`/classrooms/${context.params.classroomId}/viewersCount`);
    return viewersRef.once('value').then(data => counterRef.set(data.numChildren()));
};

updateSubscriptionsCount = (snapshot, context) => {
    const subscriptionsRef = admin.database().ref(`/subscriptions/${context.params.classroomId}`);
    const counterRef = admin.database().ref(`/classrooms/${context.params.classroomId}/subscriptionsCount`);
    return subscriptionsRef.once('value').then(data => counterRef.set(data.numChildren()));
};

createStorageAttachment = (object) => {
    const split = object.name.split("/");
    const category = split[0];
    const name = split[split.length - 1];
    const objectId = split[split.length - 2];

    if (category === "classrooms") {
        const attachmentsRef = admin.database().ref(`/attachments/${objectId}`);
        const attachment = { name: name, url: object.name, type: object.contentType, createdAt: new Date().getTime(), downloads: 0 };
        return attachmentsRef.push(attachment);
    } else if (category === "users" && object.contentType.startsWith("image")) {
        const pictureRef = admin.database().ref(`/users/${objectId}/pictureUrl`);
        return pictureRef.set(object.name);
    }
};

updateAttachmentsCount = (snapshot, context) => {
    const attachmentsRef = admin.database().ref(`/attachments/${context.params.classroomId}`);
    const counterRef = admin.database().ref(`/classrooms/${context.params.classroomId}/attachmentsCount`);
    return attachmentsRef.once("value").then(data => { return counterRef.set(data.numChildren()) })
};

deleteAttachment = (snapshot) => {
    const attachment = snapshot.val();
    return admin.storage().bucket().file(attachment.url).delete();
};

exports.updateViewersCount = functions.database.ref('/viewers/{classroomId}').onWrite(updateViewersCount);
exports.updateSubscriptionsCount = functions.database.ref('/subscriptions/{classroomId}').onWrite(updateSubscriptionsCount);
exports.deleteAttachment = functions.database.ref('/attachments/{classroomId}/{attachmentId}').onDelete(deleteAttachment);
exports.updateAttachmentsCount = functions.database.ref('/attachments/{classroomId}').onWrite(updateAttachmentsCount);
exports.createStorageAttachment = functions.storage.object().onFinalize(createStorageAttachment);
